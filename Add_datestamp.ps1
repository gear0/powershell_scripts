﻿# This script adds a datestamp to picture names.
# The month and day information is retrieved by the
# folder which contains the picture
# The foldername must be of the format ddMM (e.g. "1509" for 15th of September)
# Filenames have to start with IMG_
# The datestamp is added at the beginning of the filename and has
# the format YYYY-MM-DD

# set this variable according to your environment
$inital_directory = "C:\Users\$env:USERNAME\Desktop\DCIM"


# - DCIM
#  \
#  |- 123_1509
#  |- 234_1609
#  |- 345_2202
Function Rename-File($folder){
    $folder_content = Get-ChildItem $folder
    foreach ($directory in $folder_content){
        $path = $directory.fullname
        $sub_dir_name = $directory.name

        # TODO use capturing group
        if (!($sub_dir_name -match "^[0-9]{3}_[0-9]{4}")){
            continue
        } else{
            # build datestamp
            # TODO use capturing group
            $month_day = $sub_dir_name -replace "^[0-9]{3}_", ""
            $month = $month_day.substring(2)
            $day = $month_day.substring(0,2)
            $date = $year.Year.ToString() + "-" + $month + "-" + $day
            $sub_path = Get-ChildItem $path
            # rename files
            foreach($file in $sub_path){
                $suffix = $file -replace "IMG_", ""
                $newname = $date + "_" +  $suffix
                Rename-Item  -path $file.fullname -NewName $newname
            }
        }
    }
}


$year = Get-Date
# Backup pictures before renaming them
$backup_dir = $inital_directory + "_BACKUP"
Copy-Item -Recurse $inital_directory $backup_dir

Rename-File($inital_directory)

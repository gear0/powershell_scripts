﻿# diese Variable muss angepasst werden
$inital_directory = ""
$year = Get-Date

$backup_dir = $inital_directory + "_BACKUP"

Copy-Item -Recurse $inital_directory $backup_dir
Function Rename-File($folder){
    $folder_content = Get-ChildItem $folder
    foreach ($directory in $folder_content){
        $path = $directory.fullname
        $sub_dir_name = $directory.name

        if (!($sub_dir_name -match "^[0-9]{3}_[0-9]{4}")){
            continue
        } else{
            # build datestamp
            $month_day = $sub_dir_name -replace "^[0-9]{3}_", ""
            $month = $month_day.substring(2)
            $day = $month_day.substring(0,2)
            $date = $year.Year.ToString() + "-" + $month + "-" + $day
            $sub_path = Get-ChildItem $path
            # rename files
            foreach($file in $sub_path){
                $suffix = $file -replace "IMG_", ""
                $newname = $date + "_" +  $suffix
                Rename-Item  -path $file.fullname -NewName $newname
            }
        }
    }
}



Rename-File($inital_directory)

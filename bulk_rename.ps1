﻿# This script prompts the user for a name which is added
# to the end of the filename, but before the extension
$inital_directory = "C:\Users\$env:USERNAME\Desktop\DCIM\"

function bulk_rename(){
    $construction_site_name = display_name_input

    foreach($file in $files){
        $file_object = Get-Item $file
        $suffix = $file_object.Extension
        $newname = $file_object.Name.TrimEnd($suffix) + "_" + $construction_site_name  + $suffix
        Rename-Item -Path $file_object.FullName -NewName $newname
    }
}

function Select-FileDialog{
    param([string]$Title,[string]$Directory)
    [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
    $objForm = New-Object System.Windows.Forms.OpenFileDialog
    $objForm.InitialDirectory = $directory
    $objForm.Title = $title
    $objForm.Multiselect = $true
    $Show = $objForm.ShowDialog()
    If ($Show -eq "OK")	{
        Return $objForm.FileNames
    }else	{
        #Write-Error "Operation cancelled by user."
        exit
    }
}

function display_name_input(){
    Add-Type -AssemblyName System.Windows.Forms
    Add-Type -AssemblyName System.Drawing

    $form = New-Object System.Windows.Forms.Form
    $form.Text = "Data Entry Form"
    $form.Size = New-Object System.Drawing.Size(300,200)
    $form.StartPosition = "CenterScreen"

    $OKButton = New-Object System.Windows.Forms.Button
    $OKButton.Location = New-Object System.Drawing.Point(75,120)
    $OKButton.Size = New-Object System.Drawing.Size(75,23)
    $OKButton.Text = "OK"
    $OKButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
    $form.AcceptButton = $OKButton
    $form.Controls.Add($OKButton)

    $CancelButton = New-Object System.Windows.Forms.Button
    $CancelButton.Location = New-Object System.Drawing.Point(150,120)
    $CancelButton.Size = New-Object System.Drawing.Size(75,23)
    $CancelButton.Text = "Cancel"
    $CancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
    $form.CancelButton = $CancelButton
    $form.Controls.Add($CancelButton)

    $label = New-Object System.Windows.Forms.Label
    $label.Location = New-Object System.Drawing.Point(10,20)
    $label.Size = New-Object System.Drawing.Size(280,20)
    $label.Text = "Baustellenname:"
    $form.Controls.Add($label)

    $textBox = New-Object System.Windows.Forms.TextBox
    $textBox.Location = New-Object System.Drawing.Point(10,40)
    $textBox.Size = New-Object System.Drawing.Size(260,20)
    $form.Controls.Add($textBox)
    $form.Topmost = $True

    $form.Add_Shown({$textBox.Select()})
    $result = $form.ShowDialog()

    if ($result -eq "OK"){
        return $textBox.Text
    }else{
        exit
    }
}

$files = Select-FileDialog -title "Choose Files" -directory $inital_directory
$backup_dir = $inital_directory + "BULK_BACKUP"

if (!(Test-Path $backup_dir)){
    New-Item -ItemType directory -Path $backup_dir | Out-Null
}

# Backup selected files before renaming them
foreach($file in $files){
    Copy-Item $files -Destination $backup_dir
}

bulk_rename
